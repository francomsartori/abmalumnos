export class Alumno {
    nombre      : string;
    apellido    : string;
    dni         : string;

    constructor(nombre : string, apellido : string, dni : string){
        this.nombre     = nombre;
        this.apellido   = apellido;
        this.dni        = dni;
    }
}