import { Component, OnInit } from '@angular/core';
import { Alumno } from '../models/alumno.model';

@Component({
  selector: 'app-listado-alumnos',
  templateUrl: './listado-alumnos.component.html',
  styleUrls: ['./listado-alumnos.component.css']
})
export class ListadoAlumnosComponent implements OnInit {

  listAlumnos : Alumno[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  guardarAlumno(nombre : string, apellido : string, dni : string) : boolean {
    this.listAlumnos.unshift(new Alumno(nombre, apellido, dni));
    return false;
  }

}
