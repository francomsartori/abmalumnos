import { Component, OnInit, Input } from '@angular/core';
import { Alumno } from '../models/alumno.model';

@Component({
  selector: 'app-item-alumno',
  templateUrl: './item-alumno.component.html',
  styleUrls: ['./item-alumno.component.css']
})
export class ItemAlumnoComponent implements OnInit {

  @Input() alumno : Alumno;

  constructor() { }

  ngOnInit(): void {
  }

}
